
if [ "$1" == "apache" ] 
then
echo "
[Apache]
enabled  = true
port     = http,https
filter   = apache-auth
logpath  = /var/log/apache2/*error.log
maxretry = 3
findtime = 600
" >> /etc/fail2ban/jail.local
fi
systemctl restart apache2
